$(function () {
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 100) {
            $(".menu").addClass("bg-white nav-shadow");    
            $(".header-logo").addClass("header-color-logo");
        } else {
            $(".menu").removeClass("bg-white nav-shadow");
            $(".header-logo").removeClass("header-color-logo");
        }
    });
    $(document).on('scroll', function () {
        // if the scroll distance is greater than 100px
        if ($(window).scrollTop() > 100) {
            // do something
            $('.menu').addClass('bg-white');
        }
    });
});
$('.navbar-nav .nav-item').click(function () {
    $('.navbar-nav .nav-item.active').removeClass('active');
    $(this).addClass('active');
});

$(document).on("click", ".nav-item", function () {
    jQuery(".nav-item").closest(".bsnav-mobile").removeClass("in");
    jQuery(".toggler-spring").removeClass("active");
});

$(window).scroll(function () {
    var href = $(this).scrollTop();
    $('.link').each(function (event) {
        if (href >= $($(this).attr('href')).offset().top - 1) {
            $('.navbar-nav .nav-item.active').removeClass('active');
            $(this).addClass('active');
        }
    });
});


// Carousel
var $owl = $('.owl-carousel');

$owl.children().each(function (index) {
    $(this).attr('data-position', index); 
});

$owl.owlCarousel({
    center: true,
    loop: true,
    items: 3,
    responsive: {
        0: {
            items: 1,
            margin:25,
            nav:true,
            navText: ["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"]
        },
        768: {
            items: 3
        }
    }
});

$(document).on('click', '.owl-item>div', function () {
    var $speed = 300;  // in ms
    $owl.trigger('to.owl.carousel', [$(this).data('position'), $speed]);
});

// Para el efecto del fondo que se mueve al scrollear
$(window).on("load resize scroll", function () {
    $(".branding").each(function () {
        var windowTop = $(window).scrollTop();
        var elementTop = $(this).offset().top;
        var rightPosition = windowTop - elementTop;
        $(this)
            .find(".bg-move")
            .css({ right: rightPosition });
    });
});

AOS.init();